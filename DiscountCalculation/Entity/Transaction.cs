﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountCalculation.Entity
{
    /// <summary>
    /// Represents a single transaction.
    /// </summary>
    public class Transaction
    {
        protected DateTime date;
        protected Carrier.SizeCode sizeCode;
        protected Carrier carrier;
        protected Dictionary<string, Money> appliedDiscounts = new Dictionary<string, Money>();
        protected Money basePrice;

        public DateTime Date { get { return date; } }
        public Carrier.SizeCode SizeCode { get { return sizeCode; } }
        public Carrier Carrier { get { return carrier; } }
        public Dictionary<string, Money> AppliedDiscounts { get { return appliedDiscounts; } }
        public Money BasePrice { get { return basePrice; } }


        public Transaction(DateTime date, Carrier.SizeCode sizeCode, Carrier carrier)
        {
            this.date = date;
            this.sizeCode = sizeCode;
            this.carrier = carrier;

            // storing the original price for the transaction. Carrier may change their prices anytime so we cant rely on 
            // extracting the price from this.carrier in the future.
            try
            {
                basePrice = (Money)carrier.Prices[sizeCode].Clone();
            }
            catch (KeyNotFoundException)
            {
                throw new ArgumentException($"Provided Carrier has no price for size {sizeCode}!");
            }
        }


        /// <summary>
        /// Adds additional discount without overwriting discounts already applied for this transaction
        /// </summary>
        /// <param name="ruleName"></param>
        /// <param name="discount"></param>
        public void AddDiscount(string ruleName, Money discount)
        {
            appliedDiscounts.Add(ruleName, discount);
        }

        /// <summary>
        /// Overwrites any previously added discounts. If thats not desirable use <code>AddDiscount()</code>
        /// </summary>
        /// <param name="discounts"></param>
        public void SetDiscounts(Dictionary<string, Money> discounts)
        {
            appliedDiscounts = discounts;
        }

        /// <summary>
        /// Returns total price of the transaction including any applied discounts 
        /// </summary>
        /// <returns></returns>
        public Money GetFinalPrice()
        {
            return basePrice - GetTotalDiscount();
        }

        /// <summary>
        /// Returns sum of all discounts applied to this transaction
        /// </summary>
        /// <returns></returns>
        public Money GetTotalDiscount()
        {
            if (appliedDiscounts.Count == 0)
            {
                return Money.Zero;
            }

            decimal totalDiscountAmount = appliedDiscounts.Sum(x => x.Value.Amount);
            return new Money(totalDiscountAmount, BasePrice.Currency);
        }
    }
}
