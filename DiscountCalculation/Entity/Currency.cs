﻿using System;

namespace DiscountCalculation.Entity
{
    public class Currency
    {
        protected string code;
        protected string sign;
        protected string name;

        public string Code { get { return code; } }
        public string Sign { get { return sign; } }
        public string Name { get { return name; } }

        public Currency(string code, string name, string sign)
        {
            this.code = code;
            this.sign = sign;
            this.name = name;
        }

        public Currency(string code, string name) : this(code, name, String.Empty)
        {

        }
    }
}
