﻿using System.Collections.Generic;

namespace DiscountCalculation.Entity
{
    public class Carrier
    {
        protected string name;
        protected string shortName;
        protected Dictionary<SizeCode, Money> prices = new Dictionary<SizeCode, Money>();

        public string ShortName { get { return shortName; } }
        public string Name { get { return name; } }
        public Dictionary<SizeCode, Money> Prices { get { return prices; } }

        // Map for converting string to SizeCode; for performance reasons
        public static readonly Dictionary<char, SizeCode> sizeCodes = new Dictionary<char, SizeCode>
        {
            { 'S', SizeCode.S },
            { 'M', SizeCode.M },
            { 'L', SizeCode.L },
        };
        public enum SizeCode { S, M, L };

        public Carrier(string name, string shortName, Dictionary<SizeCode, Money> prices) : this(name, shortName)
        {
            this.prices = prices;
        }

        public Carrier(string name, string shortName)
        {
            this.name = name;
            this.shortName = shortName;
        }

        public void SetPrice(SizeCode size, Money price)
        {
            if (prices.ContainsKey(size) )
            {
                prices[size] = price;
            }
            else
            {
                prices.Add(size, price);
            }
        }
    }
}
