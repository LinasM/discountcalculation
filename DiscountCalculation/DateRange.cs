﻿using System;
using System.Collections.Generic;

namespace DiscountCalculation
{
    public static class DateRange
    {
        public enum Period { Day, Week, Month, Year };
        delegate (DateTime, DateTime) PeriodDelegate(DateTime date);
        static Dictionary<DateRange.Period, PeriodDelegate> periodsMap = new Dictionary<DateRange.Period, PeriodDelegate>
        {
            // register methods here
            {Period.Week, GetRangeWeek},
            {Period.Month, GetRangeMonth},
            {Period.Year, GetRangeYear},
        };

        // Dictionary for conversion from string, for performance reasons.
        public static Dictionary<string, Period> periodFields = new Dictionary<string, Period>
        {
            {"Day", Period.Day},
            {"Week", Period.Week},
            {"Month", Period.Month},
            {"Year", Period.Year},
        };

        public static (DateTime, DateTime) GetRange(DateTime date, Period period)
        {
            PeriodDelegate d = periodsMap[period];
            return d(date);
        }

        public static (DateTime, DateTime) GetRangeMonth(DateTime date)
        {
            DateTime startDate = new DateTime(date.Year, date.Month, 1);
            DateTime endDate = new DateTime(date.Year, date.Month, DateTime.DaysInMonth(date.Year, date.Month));

            return (startDate, endDate);
        }

        /// <summary>
        /// This method may or may not have bugs related to different locales
        /// I.e. week can start on Monday or on Sunday depending on locale
        /// As with all methods doing what was not explicitly asked by the task
        /// It's somewhat of a placeholder. It merely helps to demonstrate the implementation
        /// of flexibility: how to change " accumulated discount limit per month" to 
        /// "...per week".
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static (DateTime, DateTime) GetRangeWeek(DateTime date)
        {
            const int daysInWeek = 7;
            const int firstDayOfWeekIndex = 1;

            int weekStart = date.Day - (int)date.DayOfWeek + firstDayOfWeekIndex;

            // Constants are only used once throughout the code
            // but I simply like using constants instead of numbers where possible.
            // It makes the code more readable and less prone to 
            // introducing errors down the line
            int weekEnd = daysInWeek - (int)date.DayOfWeek + date.Day;

            DateTime startDate = new DateTime(date.Year, date.Month, weekStart);
            DateTime endDate = new DateTime(date.Year, date.Month, weekEnd);

            return (startDate, endDate);
        }

        public static (DateTime, DateTime) GetRangeYear(DateTime date)
        {
            DateTime dateStart = new DateTime(date.Year, 1, 1);
            DateTime dateEnd = new DateTime(date.Year, 12, 31);

            return (dateStart, dateEnd);
        }
    }
}
