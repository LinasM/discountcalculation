﻿using DiscountCalculation.Entity;
using System;

namespace DiscountCalculation
{
    /// <summary>
    /// I am definitely aware that Money type is not needed for this task, strictly speaking.
    /// The task only uses euros everywhere so simple decimal type would suffice.
    /// 
    /// However I cannot in good conscience write a code that passes decimals around and pretends it's money.
    /// Even if it's a throwaway test task project.
    /// 
    /// I'm trying not to get carried away too much with this so I did not implement any currency conversions, 
    /// but I did add placeholders for some of the functionality that would be needed sooner or later had this 
    /// been a real life production app
    /// </summary>
    public class Money : ICloneable
    {
        decimal amount;
        Currency currency = null;

        public decimal Amount { get { return amount; } }
        public Currency Currency { get { return currency; } }

        public static Money Zero { get { return new Money();  } }

        public Money(decimal amount, Currency currency)
        {
            this.currency = currency;
            this.amount = amount;
        }

        public Money() : this(0.0m, null)
        {
            
        }


        /*
         *   I have methods for increase and decrease amount instead of overloading operators
         *   mostly because every instance of operations between Money and numeric types 
         *   is most likely going to be unintentional mistake on my part. So I want them to throw exception 
         *   so I could spot and fix them immediately.
         */

        public void Add(decimal amount)
        {
            this.amount += amount;
        }

        public void Subtract(decimal amount)
        {
            this.amount -= amount;
        }

        public Money ConvertTo(Currency newCurrency)
        {
            if (amount == 0)
            {
                currency = newCurrency;
            }
            else if (newCurrency != currency)
            {
                throw new NotImplementedException();
            }

            return this;
        }

        /*
         * I do overload operators for operations between Money and Money. It's going to be a pretty
         * common operation so I feel like overloading operators instead of having to use special methods
         * is simpler and faster. Since both operands have the same type it's not as prone to errors,  
         * easy to read, understand whats going on, debug. However I am not 100% confident about this decision.
         * I would probably not use it in a real life important production code, at least not without discussing
         * it with the team beforehand.
         * 
         * I'm especially not sure about silently converting currency when needed. It's kinda
         * convenient but maybe a better idea is to just throw exception when currency does not match, forcing programmer 
         * to explicitly normalize currency before doing any operations. However since this task does not need 
         * any currency conversions at all I'm just not going to dedicate this issue too much time and leave it like
         * this.
         * 
         */

        public static bool operator >(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount > money2.Amount;
        }

        public static bool operator <(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount < money2.Amount;
        }

        public static bool operator ==(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount == money2.Amount;
        }

        public static bool operator !=(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount != money2.Amount;
        }

        public static bool operator >=(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount >= money2.Amount;
        }

        public static bool operator <=(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount <= money2.Amount;
        }


        public static Money operator -(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                money1.ConvertTo(money2.currency);
            }

            decimal difference = money1.Amount - money2.Amount;
            Currency currency = money1.Currency is null ? money2.Currency : money1.Currency;
            return new Money(difference, currency);
        }

        public static Money operator +(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            decimal sum = money1.Amount + money2.Amount;
            Currency currency = money1.Currency is null ? money2.Currency : money1.Currency;
            return new Money(sum, currency);
        }

        public static Money Min(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount > money2.Amount ? money2 : money1;
        }

        public static Money Max(Money money1, Money money2)
        {
            if (NeedsNormalizing(money1, money2))
            {
                Normalize(money1, money2);
            }

            return money1.Amount < money2.Amount ? money2 : money1;
        }

        /// <summary>
        /// Money needs normalizing (converting currencies and calculating new amounts accordingly)
        /// before any operation if both objects have amounts higher than zero and different currencies
        /// 
        /// This is more of a placeholder than a real method, it's only non-placeholder-ish purpose is 
        /// so I could use Money.Zero without providing currency.
        /// </summary>
        /// <param name="money1"></param>
        /// <param name="money2"></param>
        /// <returns></returns>
        public static bool NeedsNormalizing(Money money1, Money money2)
        {
            if (money1.Amount == 0.0m || money2.Amount == 0.0m)
            {
                return false;
            }

            if (money1.Currency is null || money2.Currency is null)
            {
                return true;
            }

            if (money1.Currency.Code == money2.Currency.Code)
            {
                return false;
            }

            return true;
        }

        protected bool NeedsNormalizing(Money money)
        {
            return NeedsNormalizing(this, money);
        }

        public static void Normalize(Money money1, Money money2, Currency currency)
        {
            throw new NotImplementedException();
        }

        public static void Normalize(Money money1, Money money2)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Just a clone function for having same amount of money in the same currency without 
        /// it being the same money. I.e. for remembering a price that may have change in the future.
        /// 
        /// Having a method to clone money does sound a bit funny though :)
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return new Money(amount, currency);
        }

        // Probably unecessary, but IDE complains if I dont have these:
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
