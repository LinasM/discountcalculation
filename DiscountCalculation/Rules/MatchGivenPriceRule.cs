﻿using DiscountCalculation.Entity;

namespace DiscountCalculation.Rules
{
    /// <summary>
    /// If transactions price is higher than a given price, change it's price to given price. 
    /// Discount amount is the difference between two prices.
    /// </summary>
    public class MatchGivenPriceRule : DiscountRule, IRule
    {
        protected Money flatNewPrice = null;

        public MatchGivenPriceRule(string name, Money priceToMatch) : base(name)
        {
            flatNewPrice = priceToMatch;
        }

        public MatchGivenPriceRule(string name, Money priceToMatch, bool isFinalDiscount) : this(name, priceToMatch)
        {
            this.isFinalDiscount = isFinalDiscount;
        }

        public override decimal GetDiscountedPrice(decimal price)
        {
            return flatNewPrice.Amount;
        }

        public override bool IsEligible(Transaction transaction)
        {
            return flatNewPrice < transaction.BasePrice && base.IsEligible(transaction);
        }
    }
}
