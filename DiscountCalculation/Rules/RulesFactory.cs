﻿using DiscountCalculation.Entity;
using DiscountCalculation.Rules;
using System;
using System.Collections.Generic;

namespace DiscountCalculation
{
    public class RulesFactory : IRulesFactory
    {
        List<IRule> discountRules = new List<IRule>();
        IDataProvider _dataProvider;

        public RulesFactory(IDataProvider dataProvider)
        {
            _dataProvider = dataProvider;
        }

        public List<IRule> GetRules()
        {
            if (discountRules.Count == 0)
            {
                LoadRules();
            }

            return discountRules;
        }

        /// <summary>
        /// Create your rules here.
        /// My first idea was to describe them in XML and just load here but I have up on it.
        /// It seems rules can be too different too arbitrary to have a uniform way to describe them in XML
        /// More often than not you probably still need to write a new class or extend existing one,
        /// write new methods, and so on, i.e. you still need to tweak the code so writing XML on top of that
        /// doesnt look like a good idea. Better to just add new rules in code. However I am obviously not familiar
        /// with this line of work and what kind of rules I may expect and how often they need to be changed. 
        /// Maybe describing them in XML config is a viable and good idea. Maybe you change them so often that 
        /// recompiling code each time is a terrible idea.
        /// Anyway as always, the rest of the code does not know where the rules come from 
        /// so it's not difficult to change the implementation. But for now they are just "hardcoded" of sorts.  
        /// </summary>
        void LoadRules()
        {
            Money lowestPriceS = _dataProvider.GetLowestPrice(Carrier.SizeCode.S);
            IHistory history = _dataProvider.GetHistory();

            Carrier laPoste = _dataProvider.GetCarrier("LP");
            Carrier mondialRelay = _dataProvider.GetCarrier("MR");

            MatchGivenPriceRule lowestPriceSRule = new MatchGivenPriceRule("sizeSalwaysLowestPrice", lowestPriceS, isFinalDiscount: false);
            NthTransactionRule thirdEveryMonthRule = new NthTransactionRule("3dLFreeOncePerMonth", discountCoeff: 0.0m, which: 3,
                onlyOnce: true, period: DateRange.Period.Month, history);

            lowestPriceSRule.AppliesTo(Carrier.SizeCode.S);
            lowestPriceSRule.AppliesTo(mondialRelay);
            lowestPriceSRule.AlsoAppliesTo(laPoste);
            thirdEveryMonthRule.AppliesTo(Carrier.SizeCode.L);
            thirdEveryMonthRule.AppliesTo(laPoste);

            // Order matters!
            // Rules are processed in order they are added.
            // If a rule has "IsFinalDiscount" true, no other rule will be processed afterwards
            discountRules.Add(thirdEveryMonthRule);
            discountRules.Add(lowestPriceSRule);
        }
    }
}
