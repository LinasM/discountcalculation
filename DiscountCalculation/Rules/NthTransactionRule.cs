﻿using DiscountCalculation.Entity;
using System;

namespace DiscountCalculation.Rules
{   
    /// <summary>
    /// Applies discount coeff to every Nth transaction every given period, 
    /// i.e. every 3d transaction every month
    /// 
    /// Where Nth means Nth transaction processed and not necessarily Nth transaction that period in date order.
    /// It shouldn't matter as long as transactions are processed in order by date.
    /// 
    /// I have a feeling this rule is doing too much already, maybe its a better idea to split it into two
    /// or more rules. But I dont want to get carried away with all sorts of different rules and ways to
    /// implement them my imagination can come up with, before I know if we need it. So I think the current 
    /// "rules engine" while primitive is a good start that's not too difficult to expand in the future if needed.
    /// And if it turns out it's not sufficient, it's not too difficult to refactor. As always, 
    /// the rest of the code only knows that MyRule.IsEligible(Transaction t) must return true and 
    /// MyRule.GetDiscountValue(Transaction t) gives the discount and does not care about the inner workings.
    /// </summary>
    
    public class NthTransactionRule : DiscountRule, IRule
    {
        int which = 1; // i.e. the first one is discounted
        bool onlyOnce = true;
        DateRange.Period period;
        decimal discountCoeff;

        IHistory history;        

        public NthTransactionRule(string name, decimal discountCoeff, int which, bool onlyOnce, DateRange.Period period, IHistory history) : base(name)
        {
            this.which = which;
            this.onlyOnce = onlyOnce;
            this.period = period;
            this.discountCoeff = discountCoeff;
            this.history = history;
        }

        public NthTransactionRule(string name, decimal discountCoeff, int which, bool onlyOnce, DateRange.Period period, bool isFinalDiscount, IHistory history) 
            : this(name, discountCoeff, which, onlyOnce, period, history)
        {
            this.isFinalDiscount = isFinalDiscount;
        }

        public override decimal GetDiscountedPrice(decimal price)
        {
            decimal discountedPrice = Decimal.Multiply(price, discountCoeff);
            return discountedPrice;
        }

        public override bool IsEligible(Transaction transaction)
        {
            if (!base.IsEligible(transaction))
            {
                return false;
            }

            int transactionCount = GetCount(transaction);
            bool isNth = onlyOnce ? which - transactionCount == 1 : (transactionCount+1) % which == 0;
            return  isNth;
        }

        int GetCount(Transaction transaction)
        {
            int count = 0;
            (DateTime, DateTime) dateRange = DateRange.GetRange(transaction.Date, period);
            count = history.CountTransactionRecords(dateRange, transaction.Carrier, transaction.SizeCode);
            return count;
        }
    }
}
