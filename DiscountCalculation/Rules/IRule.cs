﻿using DiscountCalculation.Entity;

namespace DiscountCalculation.Rules
{
    /// <summary>
    /// Interface for the discountRules
    /// A side note: I dont like starting interface names with I but that's widely accepted practice in C#
    /// </summary>
    public interface IRule
    {
        decimal GetDiscountValue(decimal price);
        decimal GetDiscountedPrice(decimal price);
        bool IsEligible(Transaction transaction);
        string Name { get; }

        /// <summary>
        /// If true and if this discount IS applied no further discounts will be considered.
        /// All the previously applied discounts will stay so mind the order in which discount discountRules are processed
        /// </summary>
        /// <returns></returns>
        bool IsFinalDiscount { get; }
    }
}
