﻿using DiscountCalculation.Rules;
using System.Collections.Generic;

namespace DiscountCalculation
{
    public interface IRulesFactory
    {
        List<IRule> GetRules(); 
    }
}
