﻿using DiscountCalculation.Entity;
using System.Collections.Generic;

namespace DiscountCalculation.Rules
{
    /// <summary>
    /// The rule is applied to ANY provided carrier in the list AND any provided size in the list
    /// i.e. it checks if transaction's carrier matches ANY carrier in the list AND if size matches ANY size in the list.
    /// If either appliesTo list is empty the rule won't be applied at all.
    /// Currently the only way to make it apply to EVERY carrier or EVERY size is to literally add every carrier/size to the list.
    /// If you need a different functionality for e.g. S size with LP carrier and S size with MR carrier, create 2 different instances
    /// If you need the same functionality for e.g. S size, L size and any size as long as the carrier is LP, 1 instance will suffice
    /// 
    /// </summary>
    public abstract class DiscountRule : IRule
    {
        readonly string name;

        protected List<Carrier.SizeCode> appliesToSizes = new List<Carrier.SizeCode>();
        protected List<Carrier> appliesToCarriers = new List<Carrier>();
        /// <summary>
        /// If true, if this discount is applied to a transaction, no other discounts will be
        /// Note that order in which discount discountRules are processed matters, any discounts applied before this
        /// will stay. Another way to implement it would be to also remove any previously applied discounts making 
        /// this one a truly one and only discount. However I feel like this way offers more flexibility. Yet another way
        /// would be to have a different property for a discount that's meant to be the only one ever. But I feel like 
        /// that would be too complicated and prone to bugs in the future, not easy to debug either.
        /// </summary>
        protected bool isFinalDiscount = true;
        public bool IsFinalDiscount { get { return isFinalDiscount; } }

        public string Name { get { return name; } }

        public DiscountRule(string name)
        {
            this.name = name;
        }

        public void AppliesTo(List<Carrier.SizeCode> sizes)
        {
            appliesToSizes = sizes;
        }

        public void AppliesTo(List<Carrier> carriers)
        {
            appliesToCarriers = carriers;
        }

        public void AppliesTo(Carrier carrier)
        {
            appliesToCarriers = new List<Carrier>();
            appliesToCarriers.Add(carrier);
        }

        public void AppliesTo(Carrier.SizeCode size)
        {
            appliesToSizes = new List<Carrier.SizeCode>();
            appliesToSizes.Add(size);
        }

        public void AlsoAppliesTo(Carrier.SizeCode size)
        {
            appliesToSizes.Add(size);
        }

        public void AlsoAppliesTo(List<Carrier.SizeCode> sizes)
        {
            appliesToSizes.AddRange(sizes);
        }

        public void AlsoAppliesTo(List<Carrier> carriers)
        {
            appliesToCarriers.AddRange(carriers);
        }

        public void AlsoAppliesTo(Carrier carrier)
        {
            appliesToCarriers.Add(carrier);
        }

        public decimal GetDiscountValue(decimal price)
        {
            return price - GetDiscountedPrice(price);
        }

        public abstract decimal GetDiscountedPrice(decimal price);

        /// <summary>
        /// The main method checking if given transaction is eligible for the discount
        /// The name could probably be better :)
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public virtual bool IsEligible(Transaction transaction)
        {
            return (appliesToCarriers.Contains(transaction.Carrier))
                        && (appliesToSizes.Contains(transaction.SizeCode));
        }
    }
}
