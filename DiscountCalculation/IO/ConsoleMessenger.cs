﻿using DiscountCalculation.Entity;
using System;

namespace DiscountCalculation.IO
{
    /// <summary>
    /// For writing stuff to console.
    /// </summary>
    public class ConsoleMessenger : IMessenger
    {
        protected readonly string invalidTransactionSuffix = Properties.Settings.Default.invalidLineSuffix;
        protected readonly string noDiscountSuffix = Properties.Settings.Default.noDiscountSuffix;
        protected readonly string dateFormat = Properties.Settings.Default.dateFormat;
        protected readonly string moneyFormatSpecifier = Properties.Settings.Default.decimalFormat;
        protected readonly char delimiter;

        public ConsoleMessenger()
        {
            //static char delimiter = Properties.Settings.Default.delimiter;
            delimiter = ' ';
        }

        public void WriteError(string error)
        {
            Console.WriteLine(error);
        }

        public void WriteInfo(Transaction transaction)
        {
            string infoMsg = GetTransactionInfoString(transaction);
            Console.WriteLine(infoMsg);
        }

        protected string GetTransactionInfoString(Transaction transaction)
        {
            string[] info = GetTransactionInfo(transaction);
            return string.Join(delimiter.ToString(), info);
        }

        protected string[] GetTransactionInfo(Transaction transaction)
        {
            Money totalDiscount = transaction.GetTotalDiscount();
            string[] info = new string[5]
            {
                transaction.Date.ToString(dateFormat),
                transaction.SizeCode.ToString(),
                transaction.Carrier.ShortName,
                transaction.GetFinalPrice().Amount.ToString(moneyFormatSpecifier),
                totalDiscount.Amount > 0 ? totalDiscount.Amount.ToString(moneyFormatSpecifier) : noDiscountSuffix,
            };

            return info;
        }

        public void WriteParseError(string invalidLine)
        {
            Console.WriteLine($"{invalidLine} {invalidTransactionSuffix}");
        }
    }
}
