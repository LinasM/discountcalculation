﻿using DiscountCalculation.Entity;

namespace DiscountCalculation.IO
{
    /// <summary>
    /// Interface for user messages. It's meant for writing them to console, logging in files, database, etc.
    /// A side note: I dont like starting interface names with I but that's widely accepted practice in C#
    /// </summary>
    public interface IMessenger
    {
        void WriteError(string error);
        void WriteInfo(Transaction transaction);
        void WriteParseError(string invalidLine);
    }
}
