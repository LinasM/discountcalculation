﻿using DiscountCalculation.Entity;
using DiscountCalculation.Properties;
using System;
using System.Collections.Generic;
using System.IO;

namespace DiscountCalculation.IO
{
    public class Parser
    {
        /// <summary>
        /// How many parameters is expected in a line, i.e. date, size, carrier - 3. 
        /// For quick preliminary validation. Obviously its not the only kind validation I have
        /// but if this fails there's no point in proceeding any further.
        /// </summary>
        public static readonly int parameterCount = 3;

        /// <summary>
        /// A convenient way to just switch discounts off if needed.
        /// </summary>
        public static readonly bool discountsEnabled = Settings.Default.discountsEnabled;

        readonly IDataProvider _dataProvider;
        readonly IMessenger _messenger;

        public Parser(IDataProvider dataProvider, IMessenger messenger)
        {
            _dataProvider = dataProvider;
            _messenger = messenger;
        }

        public bool TryParse(string filePath, char delimiter, DiscountManager discountManager)
        {
            try
            {
                Parse(filePath, delimiter, discountManager);
                return true;
            }
            catch (FileNotFoundException e)
            {
                _messenger.WriteError(e.Message);
                return false;
            }
        }

        public void Parse(string filePath, char delimiter, DiscountManager discountManager)
        {
            string line;
            Dictionary<string, Money> discounts = new Dictionary<string, Money>();

            using (var reader = File.OpenText(filePath))
            {
                while ((line = reader.ReadLine()) != null)
                {
                    Transaction transaction = null;

                    // Creates transaction object from a line in text file
                    if (!TryParseLine(line, _dataProvider.GetAvailableCarriers(), delimiter, out transaction))
                    {
                        _messenger.WriteParseError(line);
                        continue;
                    }
                    //------

                    if (discountsEnabled && discountManager.DiscountFundsAvailable(transaction.Date))
                    {
                        discounts = discountManager.GetDiscounts(transaction);
                        transaction.SetDiscounts(discounts);
                    }

                    _dataProvider.GetHistory().SaveTransaction(transaction);
                    _messenger.WriteInfo(transaction);
                }
            }
        }    

        public bool TryParseLine(string rawTransactionData, Dictionary<string, Carrier> availableCarriers, char delimiter, out Transaction transaction)
        {
            transaction = null;

            string[] transactionData = rawTransactionData.Split(delimiter);

            if (transactionData.Length != parameterCount)
            {
                return false;
            }

            string dateString = transactionData[0];
            string sizeCodeString = transactionData[1];
            string carrierCodeString = transactionData[2];

            DateTime date;
            char sizeCodeChar = Char.Parse(sizeCodeString);

            bool dateIsValid = DateTime.TryParse(dateString, out date);
            bool sizeCodeIsValid = Carrier.sizeCodes.ContainsKey(sizeCodeChar);
            bool carrierCodeIsValid = availableCarriers.ContainsKey(carrierCodeString);

            if (dateIsValid && sizeCodeIsValid && carrierCodeIsValid)
            {
                transaction = new Transaction(date, Carrier.sizeCodes[sizeCodeChar], 
                    availableCarriers[carrierCodeString]);
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
