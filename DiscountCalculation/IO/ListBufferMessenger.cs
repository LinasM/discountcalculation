﻿using System.Collections.Generic;
using DiscountCalculation.Entity;


namespace DiscountCalculation.IO
{
    /// <summary>
    /// I only use this in automated tests. It extends ConsoleMessenger because it's meant to be used as 
    /// some sort of it's mock in testing ConsoleMessenger. However it may have other uses too. Probably.
    /// </summary>
    public class ListBufferMessenger : ConsoleMessenger, IMessenger
    {
        List<string> buffer = new List<string>();
        public List<string> Buffer {  get { return buffer; } }

        new public void WriteError(string error)
        {
            buffer.Add(error);
        }

        new public void WriteInfo(Transaction transaction)
        {
            string infoMsg = GetTransactionInfoString(transaction);
            buffer.Add(infoMsg);
        }

        new public void WriteParseError(string invalidLine)
        {
            buffer.Add($"{invalidLine} {invalidTransactionSuffix}");
        }
    }
}
