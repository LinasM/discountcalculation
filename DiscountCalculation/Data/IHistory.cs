﻿using System;
using System.Collections.Generic;

namespace DiscountCalculation.Entity
{
    /// <summary>
    /// Interface for retrieving and saving _history to what would likely be a database in real production.
    /// A side note: I dont like starting interface names with I but that's widely accepted practice in C#
    /// </summary>
    public interface IHistory
    {
        int CountTransactionRecords((DateTime, DateTime) dateRange);
        int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier carrier);
        int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier carrier, Carrier.SizeCode sizeCode);
        int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier.SizeCode sizeCode);
        List<Transaction> GetDiscountedTransactionsRecords((DateTime, DateTime) dateRange);
        void SaveTransaction(Transaction transaction);
        void SaveTransactions(List<Transaction> transactions);
    }
}