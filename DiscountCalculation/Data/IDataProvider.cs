﻿using DiscountCalculation.Entity;
using System.Collections.Generic;

namespace DiscountCalculation
{
    /// <summary>
    /// Interface for what would be a database interaction class in real production
    /// A side note: I dont like starting interface names with I but that's widely accepted practice in C#
    /// </summary>
    public interface IDataProvider
    {
        Dictionary<string, Carrier> GetAvailableCarriers();
        Carrier GetCarrier(string carrierCode);
        Dictionary<string, Currency> GetAvailableCurrencies();
        Currency GetCurrency(string currencyCode);
        Money GetLowestPrice(Carrier.SizeCode sizeCode);
        IHistory GetHistory();
    }
}
