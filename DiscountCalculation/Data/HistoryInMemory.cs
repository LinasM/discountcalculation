﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountCalculation.Entity
{    
    /// <summary>
    /// Stores transaction _history in memory while program is being executed. It's a mock of some kind of 
    /// class that would manage transaction _history in database, i.e. load, save, etc.
    /// 
    /// Normally I'd probably use SQLite for a throwaway project like this but then I'd need a third party ADO.NET provider 
    /// and the task description explicitly forbids using external libraries.
    /// </summary>
    public class HistoryInMemory : IHistory
    {
        protected List<Transaction> transactionRecords = new List<Transaction>();

        public List<Transaction> GetDiscountedTransactionsRecords((DateTime, DateTime) dateRange)
        {
            return transactionRecords.Where(transaction => transaction.Date >= dateRange.Item1)
                .Where(transaction => transaction.Date <= dateRange.Item2)
                .Where(transaction => transaction.GetTotalDiscount() > Money.Zero).ToList();
        }

        public int CountTransactionRecords((DateTime, DateTime) dateRange)
        {
            return transactionRecords.Where(transaction => transaction.Date >= dateRange.Item1)
                .Where(transaction => transaction.Date <= dateRange.Item2).Count();
        }

        public int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier carrier)
        {
            return transactionRecords.Where(transaction => transaction.Date >= dateRange.Item1)
                .Where(transaction => transaction.Date <= dateRange.Item2)
                .Where(transaction => transaction.Carrier == carrier).Count();
        }

        public int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier.SizeCode sizeCode)
        {
            return transactionRecords.Where(transaction => transaction.Date >= dateRange.Item1)
                .Where(transaction => transaction.Date <= dateRange.Item2)
                .Where(transaction => transaction.SizeCode == sizeCode).Count();
        }

        public int CountTransactionRecords((DateTime, DateTime) dateRange, Carrier carrier, Carrier.SizeCode sizeCode)
        {
            return transactionRecords.Where(transaction => transaction.Date >= dateRange.Item1)
                    .Where(transaction => transaction.Date <= dateRange.Item2)
                    .Where(transaction => transaction.Carrier == carrier)
                    .Where(transaction => transaction.SizeCode == sizeCode).Count();
        }

        public void SaveTransaction(Transaction transaction)
        {
            transactionRecords.Add(transaction);
        }

        public void SaveTransactions(List<Transaction> transactions)
        {
            transactionRecords.AddRange(transactions);
        }
    }
}
