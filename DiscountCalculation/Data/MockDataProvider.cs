﻿using DiscountCalculation.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DiscountCalculation.Data
{
    /// <summary>
    /// Some kind of mock of a class responsible to writing to or retrieving from a database. I use it for things 
    /// that would likely go into database in a real life production code: list of carriers (table carriers), list of
    /// supported currencies, list of prices (many to many relation between carriers and sizes with prices at a join table)
    /// transaction history, and so on. 
    /// 
    /// Everything is actually hardcoded for the purposes of this task, since the task explicitly forbids using a third party
    /// library, normally I'd use SQLite for this kind of throwaway project, but C# needs a library for that.
    /// 
    /// However code everywhere knows only about IDataProvider interface so changing this mock to a real DB wrapper class
    /// implementing the same interface should be very simple. The "business logic" code does not need to know where the data
    /// comes from and it cannot (and must not!) affect it's behavior in any way.
    /// 
    /// Namespace DiscountCalculation.Data is where such mock data classes reside and it's the only place ever where
    /// something is hardcoded in this project. Because it pretends the data is coming from somewhere else. I guess I just 
    /// want to make very clear that I do not think hardcoding prices is ever a good idea in a production :)
    /// </summary>
    public class MockDataProvider : IDataProvider
    {
        readonly Dictionary<string, Carrier> carriers;

        readonly Dictionary<string, Currency> currencies = new Dictionary<string, Currency>
        {
            { "EUR", new Currency("EUR", "Euro", "€")},
            { "GBP", new Currency("GBP", "Pound sterling", "£") },
        };

        readonly Dictionary<Carrier.SizeCode, Money> mrPrices;
        readonly Dictionary<Carrier.SizeCode, Money> lpPrices;

        readonly IHistory history;

        public MockDataProvider()
        {
            mrPrices = new Dictionary<Carrier.SizeCode, Money>
            {
                { Carrier.SizeCode.S, new Money(2.0m, currencies["EUR"]) },
                { Carrier.SizeCode.M, new Money(3.0m, currencies["EUR"]) },
                { Carrier.SizeCode.L, new Money(4.0m, currencies["EUR"]) },
            };

            lpPrices = new Dictionary<Carrier.SizeCode, Money>
            {
                { Carrier.SizeCode.S, new Money(1.50m, currencies["EUR"]) },
                { Carrier.SizeCode.M, new Money(4.90m, currencies["EUR"]) },
                { Carrier.SizeCode.L, new Money(6.90m, currencies["EUR"]) },
            };

            Carrier laPoste = new Carrier("La Poste", "LP", lpPrices);
            Carrier mondialRelay = new Carrier("Mondial Relay", "MR", mrPrices);

            carriers = new Dictionary<string, Carrier>
            {
                {"LP", laPoste},
                {"MR", mondialRelay},
            };

            history = new HistoryInMemory();
        }

        public Dictionary<string, Carrier> GetAvailableCarriers()
        {
            return carriers;
        }

        public Dictionary<string, Currency> GetAvailableCurrencies()
        {
            return currencies;
        }

        public Carrier GetCarrier(string carrierCode)
        {
            if (!carriers.ContainsKey(carrierCode))
            {
                throw new ArgumentException($"Carrier with a code {carrierCode} is not among available carriers");
            }

            return carriers[carrierCode];
        }

        public Currency GetCurrency(string currencyCode)
        {
            if (!currencies.ContainsKey(currencyCode))
            {
                throw new Exception($"Currency with a code {currencyCode} is not among available currencies");
            }

            return currencies[currencyCode];
        }

        /// <summary>
        /// Finds the lowest price for a given size among all available carriers.
        /// Probably not the most efficient implementation speed wise, but it's just a mock data provider.
        /// Real life production implementation would probably use some sort of database and possibly cache
        /// </summary>
        /// <param name="sizeCode">Size code (S, L, M)</param>
        /// <returns>Lowest price</returns>
        public Money GetLowestPrice(Carrier.SizeCode sizeCode)
        {
            KeyValuePair<string, Carrier> carrierWithLowestPrice = carriers.Aggregate((i1, i2) => i1.Value.Prices[sizeCode] < i2.Value.Prices[sizeCode] ? i1 : i2);
            return carrierWithLowestPrice.Value.Prices[sizeCode];
        }

        public IHistory GetHistory()
        {
            return history;
        }
    }
}
