﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.IO;
using System;
using System.Collections.Generic;

namespace DiscountCalculation
{
    class Program
    {
        static readonly string defaultCurrencyCode = Properties.Settings.Default.defaultCurrency;
        static readonly string exitMessage = Properties.Settings.Default.exitMessage;

        static readonly string discountLimitPeriod = Properties.Settings.Default.discountLimitPeriod;
        static readonly decimal discountLimitAmount = Properties.Settings.Default.discountLimitAmount;

        static void Main(string[] args)
        {
            string filePath = args.Length > 0 ? args[0] : Properties.Settings.Default.filePath;
            char delimiter = ' ';

            IMessenger messenger = new ConsoleMessenger();
            IDataProvider dataProvider = new MockDataProvider();

            Dictionary<string, Currency> availableCurrencies = dataProvider.GetAvailableCurrencies();

            if (!availableCurrencies.ContainsKey(defaultCurrencyCode))
            {
                messenger.WriteError($"Currency code {defaultCurrencyCode} is not among available currencies");
                return;
            }

            Currency defaultCurrency = dataProvider.GetCurrency(defaultCurrencyCode);
            Money discountLimit = new Money(discountLimitAmount, defaultCurrency);
            DateRange.Period period = DateRange.periodFields[discountLimitPeriod];
            (DateRange.Period, Money) limit = (period, discountLimit);
            IRulesFactory rulesFactory = new RulesFactory(dataProvider);

            DiscountManager discountManager = new DiscountManager(rulesFactory.GetRules(), 
                dataProvider.GetHistory(), limit, defaultCurrency);
            Parser parser = new Parser(dataProvider, messenger);
            try
            {
                parser.TryParse(filePath, delimiter, discountManager);
            }
            catch(Exception e)
            {
                messenger.WriteError(e.Message);
            }

#if DEBUG
            Console.Write($"\r\n\r\n{exitMessage}");
            Console.ReadKey();
#endif
        }
    }
}
