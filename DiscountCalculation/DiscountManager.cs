﻿using DiscountCalculation.Entity;
using DiscountCalculation.Rules;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace DiscountCalculation
{

    /// <summary>
    /// Responsible for managing discounts: processing discount discountRules, checking if discounts are still available
    /// (i.e. not turned off in config and not exceeding limit per month, etc).
    /// </summary>
    public sealed class DiscountManager
    {
        // settings
        readonly Currency defaultCurrency;
        readonly (DateRange.Period Period, Money Sum) limit;
        /// <summary>
        /// If there are not enough funds to fully cover a discount this calendar month, should it be covered partially?
        /// </summary>
        readonly bool givePartialDiscounts;
        
        readonly IHistory _history;

        // discount discountRules
        List<IRule> discountRules = new List<IRule>();
        
        public DiscountManager(IHistory history, (DateRange.Period, Money) limit, Currency defaultCurrency)
        {
            _history = history;
            this.defaultCurrency = defaultCurrency;
            this.limit = limit;
            givePartialDiscounts = true; // maybe move to config file
        }

        public DiscountManager(List<IRule> discountRules, IHistory history,
            (DateRange.Period, Money) limit, Currency defaultCurrency) : this(history, limit, defaultCurrency)
        {
            this.discountRules = discountRules;
        }

        public DiscountManager(IRule discountRule, IHistory history,
            (DateRange.Period, Money) limit, Currency defaultCurrency) : this(history, limit, defaultCurrency)
        {
            discountRules.Add(discountRule);
        }

        public bool DiscountFundsAvailable(DateTime date)
        {
            return limit.Sum > GetAccumulatedDiscounts(limit.Period, date);
        }

        /// <summary>
        /// Validates given transaction against every registered rule and returns cumulative discount for that transaction
        /// </summary>
        /// <param name="transaction"></param>
        /// <returns></returns>
        public Dictionary<string, Money> GetDiscounts(Transaction transaction)
        {
            Dictionary<string, Money> discounts = new Dictionary<string, Money>();
            Money discount;

            Money accumulatedDiscounts = GetAccumulatedDiscounts(transaction.Date);
            
            // Stores discounts returned by different rules for this transaction, that are not yet applied and saved
            Money accumulatedTransactionDiscounts = Money.Zero;

            Money discountFunds = limit.Sum - accumulatedDiscounts;

            foreach (IRule rule in discountRules)
            {
                if (rule.IsEligible(transaction))
                {
                    decimal discountValue = rule.GetDiscountValue(transaction.BasePrice.Amount);
                    discount = new Money(discountValue, transaction.BasePrice.Currency);

                    if (discount + accumulatedTransactionDiscounts >= discountFunds)
                    {
                        if (givePartialDiscounts)
                        {
                            discount = limit.Sum - accumulatedDiscounts - accumulatedTransactionDiscounts;
                        }
                        else
                        {
                            // Discount limit exceeded, we do not add this discount and further discountRules processing wouldnt do anything
                            break;
                        }
                    }
                    
                    discounts.Add(rule.Name, discount);
                    accumulatedTransactionDiscounts += discount;

#if DEBUG
                    // This is just a sanity check. Under no circumstances accumulated discounts can exceed limit per month 
                    // it can only match limit per month. If this assertion fails something is really wrong somehwere.
                    Debug.Assert((accumulatedDiscounts + accumulatedTransactionDiscounts) <= limit.Sum);
#endif
                    // cases where further rule processing is does not do anything or is not desirable
                    if (rule.IsFinalDiscount || (accumulatedDiscounts + accumulatedTransactionDiscounts) == limit.Sum)
                    {
                        break;
                    }
                }
            }

            return discounts;
        }

        public Money GetAccumulatedDiscounts(DateTime transactionDate)
        {
            return GetAccumulatedDiscounts(limit.Period, transactionDate);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="period"></param>
        /// <param name="transactionDate"></param>
        /// <returns></returns>
        public Money GetAccumulatedDiscounts(DateRange.Period period, DateTime transactionDate)
        {
            (DateTime, DateTime) dateRange = DateRange.GetRange(transactionDate, period);

            List<Transaction> discountRecords = _history.GetDiscountedTransactionsRecords(dateRange);

            Money totalDiscounts = Money.Zero;

            foreach (Transaction transaction in discountRecords)
            {
                // Note that ConvertTo() is not actually implemented and only "works" when the amount is zero 
                // or when the currency is already the same. It's just a placeholder should we need currency
                // conversion in the future.
                totalDiscounts += transaction.GetTotalDiscount().ConvertTo(defaultCurrency);
            }

            return totalDiscounts;
        }

        public void SetRules(List<IRule> discountRules)
        {
            this.discountRules = discountRules;
        }
    }
}
