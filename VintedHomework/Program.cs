﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;

namespace DiscountCalculation
{
    class Program
    {
        // shipping providers: France: ether 'Mondial Relay' (MR in short) or 'La Poste' (LP).

        // Size: S - Small, a popular option to ship jewelry M - Medium - clothes and similar items L - Large - mostly shoes

        // Shipping price depends on package size and a provider:

        // * All S shipments should always match the lowest S package price among the providers.

        // * Third L shipment via LP should be free, but only once a calendar month.

        // * Accumulated discounts cannot exceed 10 € in a calendar month.
        //  If there are not enough funds to fully cover a discount this calendar month, it should be covered partially.

        private static string inputFilename = "input.txt";
        private static string inputDir = "C:\\Users\\Linas\\Desktop";


        static void Main(string[] args)
        {
            // TODO a way to provide filepath as an argument in console
            string filepath = Path.Combine(inputDir, inputFilename);

            try
            {
                TransactionsProvider tProvider = new TransactionsProviderFile(filepath);
                List<Transaction> transactions = tProvider.GetTransactions();

                string date;
                string packageSizeCode;
                string carrierCode;

                string outputLine;

                foreach (Transaction transaction in transactions)
                {
                    date = transaction.GetDate().ToString();
                    packageSizeCode = transaction.GetPackageSize().ToString();
                    carrierCode = transaction.GetCarrier().Code;

                    outputLine = date = date + " " + packageSizeCode + " " + carrierCode;

                    if ( !transaction.IsValid )
                    {
                        outputLine = outputLine + " Ignored";
                    }

                    Console.WriteLine(outputLine);
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File " + filepath + " not found");
            }


            Console.ReadLine();
        }
    }
}
