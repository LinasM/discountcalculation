﻿using System;
using System.Runtime.Serialization;

namespace DiscountCalculation
{
    [Serializable]
    internal class InvalidCarrierCodeException : Exception
    {
        public InvalidCarrierCodeException()
        {

        }

        public InvalidCarrierCodeException(string message) : base(message)
        {
        }

        public InvalidCarrierCodeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidCarrierCodeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}