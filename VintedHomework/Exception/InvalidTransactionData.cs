﻿using System;
using System.Runtime.Serialization;

namespace DiscountCalculation
{
    [Serializable]
    internal class InvalidTransactionData : Exception
    {
        public string Line { get; set; }

        public InvalidTransactionData(string message, string line) : base(message)
        {
            Line = line;
        }

        public InvalidTransactionData(string message, Exception innerException) : base(message, innerException)
        {

        }

        protected InvalidTransactionData(SerializationInfo info, StreamingContext context) : base(info, context)
        {

        }
    }
}