﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Carrier : Transport
    {
        public string Code { get; }
        public string name;

        Dictionary<Transaction.PackageSize, Money> prices;

        public Carrier(string name, string code, Dictionary<Transaction.PackageSize, Money> prices)
        {
            Code = code;
            this.name = name;
            this.prices = prices;
        }

        public Money GetPrice(Transaction.PackageSize packageSize)
        {
            // todo check if exists?
            return prices[packageSize];
        }

        public Dictionary<Transaction.PackageSize, Money> GetPrices()
        {
            return prices;
        }
    }
}
