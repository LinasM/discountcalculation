﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Delivery
    {
        string destination; // a symbol of country to, i.e. FR or something like that
        string origin; // a symbol of country from, i.e. FR or something like that

        static Dictionary<string, Carrier> carriers;
        public enum CarrierCodes { MR, LP };

        public static Transport GetCarrier(string code)
        {
            // MOCK. TODO refactor

            if ( carriers is null )
            {
                Currency euros = new Currency("EUR", "Euro", "€");
                Dictionary <Transaction.PackageSize, Money> lpPrices = new Dictionary<Transaction.PackageSize, Money> 
                {
                    { Transaction.PackageSize.L, new Money(6.90m, euros) },
                    { Transaction.PackageSize.M, new Money(4.90m, euros) },
                    { Transaction.PackageSize.S, new Money(1.50m, euros) },
                };

                Dictionary<Transaction.PackageSize, Money> mrPrices = new Dictionary<Transaction.PackageSize, Money>
                {
                    { Transaction.PackageSize.L, new Money(4m, euros) },
                    { Transaction.PackageSize.M, new Money(3m, euros) },
                    { Transaction.PackageSize.S, new Money(2m, euros) },
                };

                carriers = new Dictionary<string, Carrier>
                {
                    { "LP", new Carrier("La Poste", "LP", lpPrices) },
                    { "MR", new Carrier("Mondial Relay", "MR", mrPrices) }
                };
            }

            // TODO use enum?
            if ( !carriers.ContainsKey(code) )
            {
                throw new InvalidCarrierCodeException();
            }
            
            return carriers[code];
        }
    }
}
