﻿using DiscountCalculation;
using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Money
    {
        public decimal Amount { get; }
        public Currency Currency { get; }

        public Money(decimal amount, Currency currency)
        {
            Amount = amount;
            Currency = currency;
        }
    }
}
