﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Currency
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public string Sign { get; set; }

        public Currency(string code, string name, string sign)
        {
            Code = code;
            Name = name;
            Sign = sign;
        }
    }
}
