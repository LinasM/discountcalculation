﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Vinty
    {
        decimal accumulatedDiscountThisMonth = 1;

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public Vinty(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
        
        public decimal GetAccumulatedDiscount()
        {
            return accumulatedDiscountThisMonth;
        }

    }
}
