﻿using System.Collections.Generic;

namespace DiscountCalculation
{
    interface TransactionsProvider
    {
        public List<Transaction> GetTransactions();
    }
}
