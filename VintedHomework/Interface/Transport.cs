﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    interface Transport // TODO rename remove I 
    {
        public Dictionary<Transaction.PackageSize, Money> GetPrices();
        public Money GetPrice(Transaction.PackageSize packageSize);

        public string Code { get;  }
    }
}
