﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DiscountCalculation
{
    class TransactionsProviderFile : TransactionsProvider
    {
        private string filePath;
        private readonly Char delimiter = ' ';

        public TransactionsProviderFile(string filePath)
        {
            this.filePath = filePath;
        }

        public List<Transaction> GetTransactions()
        {
            string line;
            List<Transaction> transactions = new List<Transaction>();

            using ( var reader = File.OpenText(filePath) )
            {

                while ((line = reader.ReadLine()) != null)
                {
                    string[] values = line.Split(delimiter); // TODO trim ?

                    // 1. date 2. package size code // 3. carrier code

                    try
                    {
                        string dateString = values[0];
                        char packageSizeCode = Char.Parse(values[1]);
                        string carrierCode = values[2];

                        DateTime date = DateTime.Parse(dateString);
                        Transaction.PackageSize packageSize = Transaction.packageSizes[packageSizeCode];
                        Transport carrier = Delivery.GetCarrier(carrierCode);

                        transactions.Add(new Transaction(date, packageSize, carrier));
                    }
                    catch (Exception e)
                    {
                        transactions.Add(new Transaction(isValid:false));
                    }

                }
            }

            return transactions;
        }
    }
}
