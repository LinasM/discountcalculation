﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscountCalculation
{
    class Transaction
    {
        public enum PackageSize { S, M, L }; // TODO make a class?

        // Map for performance reasons, Enum.Parse is slow
        public static readonly Dictionary<char, PackageSize> packageSizes = new Dictionary<char, PackageSize> 
        {
            {'S', PackageSize.S},
            {'M', PackageSize.M},
            {'L', PackageSize.L},
        };

        DateTime date;
        Transport carrier;
        PackageSize packageSize;

        public bool IsValid { get; set; }

        public Transaction(DateTime date, PackageSize packageSize, Transport carrier)
        {
            this.date = date;
            this.packageSize = packageSize;
            this.carrier = carrier;
        }

        public Transaction(bool isValid)
        {
            IsValid = isValid;
        }

        public Transport GetCarrier()
        {
            return carrier;
        }

        public PackageSize GetPackageSize()
        {
            return packageSize;
        }

        public DateTime GetDate()
        {
            return date;
        }
    }
}
