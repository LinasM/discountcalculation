﻿using DiscountCalculation.Entity;
using DiscountCalculation.IO;
using System;
using System.Collections.Generic;
using Xunit;
using Assert = Xunit.Assert;

namespace DiscountCalculation.Tests
{
    public class ConsoleMessengerTests
    {
        [Fact]
        public void GetTransactionInfo_Test()
        {
            #region setup
            ListBufferMessenger messenger = new ListBufferMessenger(); // mock that extends console messenger:
            Assert.True(messenger is ConsoleMessenger);
            string delimiter = " "; // comes from settings but for now hardcoded

            DateTime date = new DateTime(2001, 12, 31);
            Currency litai = new Currency("lt", "Lietuvos litai");
            Carrier.SizeCode sizeCode = Carrier.SizeCode.L;

            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                {sizeCode, new Money(100.00m,  litai)},
            };
            Carrier lietuvosPastas = new Carrier("Lietuvos paštas", "LTP", prices);
            Transaction testTransaction = new Transaction(date, sizeCode, lietuvosPastas);
            #endregion

            string[] transactionData = new string[5]
            {
                date.ToString("yyy-MM-dd"), // comes from settings, refactor maybe
                sizeCode.ToString(),
                lietuvosPastas.ShortName,
                testTransaction.GetFinalPrice().Amount.ToString("F"), // comes from settings
                "-", // comes from settings, refactor maybe
            };

            string expectedLine = string.Join(delimiter, transactionData);

            messenger.WriteInfo(testTransaction);
            string actualLine = messenger.Buffer[0];
 
            Assert.Equal(expectedLine, actualLine);
        }
    }
}
