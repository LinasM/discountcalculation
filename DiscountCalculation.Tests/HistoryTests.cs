﻿using DiscountCalculation.Entity;
using System;
using System.Collections.Generic;
using Xunit;

namespace DiscountCalculation.Tests
{
    public class HistoryTests
    {
        Currency euro = new Currency("EUR", "EURO");

        [Fact]
        public void GetRangeWeek_Test()
        {
            DateTime date = new DateTime(2020, 8, 5); // Wednesday

            DateTime weekStartsAt = new DateTime(2020, 8, 3); // Monday
            DateTime weekEndsAt = new DateTime(2020, 8, 9); // Sunday

            (DateTime, DateTime) expectedRange = (weekStartsAt, weekEndsAt);
            (DateTime, DateTime) actualRange = DateRange.GetRangeWeek(date);

            Assert.Equal(expectedRange, actualRange);
        }

        [Fact]
        public void GetTransactionRecords_TestDateRange()
        {
            #region setup
            DateTime date = new DateTime(2020, 8, 5); // Wednesday
            DateTime weekStartsAt = new DateTime(2020, 8, 3); // Monday
            DateTime weekEndsAt = new DateTime(2020, 8, 9); // Sunday

            (DateTime, DateTime) aWeek = DateRange.GetRange(date, DateRange.Period.Week);

            DateTime historicalDate = new DateTime(2020, 8, 4); // Tuesday same week
            Carrier mockCarrier = new Carrier("Mock Carrier", "MC");
            mockCarrier.SetPrice(Carrier.SizeCode.L, new Money(3.0m, euro));

            Transaction oldTransaction = new Transaction(date, Carrier.SizeCode.L, mockCarrier);
            Transaction historicalTransaction = new Transaction(historicalDate, Carrier.SizeCode.L, mockCarrier);
            oldTransaction.AddDiscount("testDiscount", new Money(2.00m, euro));

            IHistory history = new HistoryInMemory();
            history.SaveTransaction(oldTransaction);
            history.SaveTransaction(historicalTransaction);
            #endregion

            List<Transaction> transactionHistory = history.GetDiscountedTransactionsRecords(aWeek);
            Assert.NotNull(transactionHistory);

            int actualCount = transactionHistory.Count;
            int expectedCount = 1;

            Assert.Equal(expectedCount, actualCount);
            Assert.Equal(oldTransaction, transactionHistory[0]);
        }
    }
}
