﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.IO;
using DiscountCalculation.Rules;
using System.Collections.Generic;
using System.IO;
using Xunit;

namespace DiscountCalculation.Tests
{
    public class ParserTests
    {
        List<string> validTestRows = new List<string> {
                "2015-03-01 S MR",
                "2015-02-12 M MR",
                "2015-02-07 L MR",

                "2015-02-05 S LP",
                "2015-02-13 M LP",
                "2015-02-17 L LP",
            };

        Currency monopolyMoney = new Currency("MM", "Monopoly money", "$");

        [Fact]
        public void Parse_NonExistantFile()
        {
            IDataProvider dataProvider = new MockDataProvider();
            IMessenger messenger = new ListBufferMessenger();
            Parser parser = new Parser(dataProvider, messenger);
            (DateRange.Period, Money) limit = (DateRange.Period.Month, Money.Zero);

            DiscountManager discountManager = new DiscountManager(dataProvider.GetHistory(), limit, monopolyMoney);

            Assert.Throws<FileNotFoundException>(() => parser.Parse("totally_nonexistant_filepath_that_does_not_exist.txt", 
                delimiter: ' ', discountManager));
        }


        [Fact]
        public void TryParse_ValidData()
        {
            bool actual = true;
            Transaction lastCheckedTransaction = null;
            MockDataProvider dataProvider = new MockDataProvider();
            IMessenger messenger = new ListBufferMessenger();
            Parser parser = new Parser(dataProvider, messenger);
            char delimiter = ' ';


            foreach (string validRow in validTestRows)
            {
                if (!parser.TryParseLine(validRow, dataProvider.GetAvailableCarriers(), delimiter, out lastCheckedTransaction))
                {
                    actual = false;
                    break;
                }

                Assert.NotNull(lastCheckedTransaction);
            }

            Assert.True(actual);
        }
    }
}
