﻿using DiscountCalculation.Entity;
using System.Collections.Generic;
using Xunit;

namespace DiscountCalculation.Tests.Entity
{
    public class CarrierTests
    {
        Currency monopolyMoney = new Currency("Mono", "Monopoly money", "$");

        [Fact]
        public void Initialization_Test()
        {
            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                { Carrier.SizeCode.L, new Money(10.00m, monopolyMoney) },
            };

            string name = "Fake carrier";
            string shortName = "FC";

            Carrier fakeCarrier = new Carrier(name, shortName, prices);
            Carrier fakeCarrierNoPrices = new Carrier(name, shortName);


            Assert.Equal(fakeCarrier.Name, name);
            Assert.Equal(fakeCarrier.ShortName, shortName);
            Assert.Equal(fakeCarrier.Prices, prices);


            Assert.Equal(fakeCarrierNoPrices.Name, name);
            Assert.Equal(fakeCarrierNoPrices.ShortName, shortName);
            Assert.Equal(fakeCarrierNoPrices.Prices, new Dictionary<Carrier.SizeCode, Money>());
        }

        [Fact]
        public void SetPrice_Test()
        {
            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                { Carrier.SizeCode.L, new Money(10.00m, monopolyMoney) },
            };

            Carrier fakeCarrier = new Carrier("Fake carrier", "FC", prices);
            fakeCarrier.SetPrice(Carrier.SizeCode.M, new Money(12.00m, monopolyMoney));
            int expectedPricesCount = 2;
            decimal expectedAmountM = 12.00m;
            Currency expectedCurrencyM = monopolyMoney;

            Assert.Equal(expectedPricesCount, fakeCarrier.Prices.Count);
            Assert.Equal(expectedAmountM, fakeCarrier.Prices[Carrier.SizeCode.M].Amount);
            Assert.Equal(expectedCurrencyM, fakeCarrier.Prices[Carrier.SizeCode.M].Currency);
        }
    }
}
