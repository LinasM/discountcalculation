﻿using System;
using Xunit;
using DiscountCalculation.Entity;
using DiscountCalculation.Data;
using System.Collections.Generic;

namespace DiscountCalculation.Tests
{
    public class TransactionTests
    {
        IDataProvider dataProvider;
        Currency euro;

        public TransactionTests()
        {
            dataProvider = new MockDataProvider();
            euro = dataProvider.GetCurrency("EUR");
        }

        [Fact]
        public void Initalization_Test()
        {
            DateTime date = new DateTime(2000, 02, 12);
            Carrier.SizeCode sizeSmall = Carrier.SizeCode.S;
            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                { sizeSmall, new Money(1.0m, euro) },
            };
            Carrier lietuvosPastas = new Carrier("Lietuvos paštas", "LTP", prices);

            Transaction testTransaction = new Transaction(date, sizeSmall, lietuvosPastas);

            Assert.Equal(testTransaction.Date, date);
            Assert.Equal(testTransaction.Carrier, lietuvosPastas);
            Assert.Equal(testTransaction.SizeCode, sizeSmall);
        }

        [Fact]
        public void GetTotalDiscount_TestSingleDiscount()
        {
            Transaction transaction = new Transaction(DateTime.Now, Carrier.SizeCode.M, dataProvider.GetCarrier("MR"));
            Money discount = new Money(0.5m, euro);
            transaction.AddDiscount("Testing discounts", discount);

            Money expectedTotalDiscount = new Money(0.5m, euro);
            Money actualTotalDiscount = transaction.GetTotalDiscount();

            Assert.True(expectedTotalDiscount == actualTotalDiscount);
        }

        [Fact]
        public void GetTotalDiscount_TestMultipleDiscounts()
        {
            Transaction transaction = new Transaction(DateTime.Now, Carrier.SizeCode.M, dataProvider.GetCarrier("MR"));
            Money discount1 = new Money(0.5m, euro);
            Money discount2 = new Money(0.2m, euro);

            transaction.AddDiscount("testingOneDiscount", discount1);
            transaction.AddDiscount("testingAnotherDiscount", discount2);

            Money expectedTotalDiscount = new Money(0.7m, euro);
            Money actualTotalDiscount = transaction.GetTotalDiscount();
            Assert.True(expectedTotalDiscount == actualTotalDiscount);
        }

        [Fact]
        public void GetFinalPrice_Test()
        {
            Transaction transaction = new Transaction(DateTime.Now, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));
            Money discount = new Money(0.5m, dataProvider.GetCurrency("EUR"));
            transaction.AddDiscount("Testing discounts", discount);

            Money expectedPrice = transaction.BasePrice - discount;
            decimal expectedPriceAmount = 1.0m;
            Money actualPrice = transaction.GetFinalPrice();

            Assert.True(expectedPrice == actualPrice);
            Assert.Equal(expectedPrice.Amount, expectedPriceAmount);
            Assert.Equal(expectedPrice.Amount, expectedPriceAmount);
        }
    }
}
