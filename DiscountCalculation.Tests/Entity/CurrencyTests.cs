﻿using DiscountCalculation.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DiscountCalculation.Tests.Entity
{
    public class CurrencyTests
    {
        [Fact]
        public void Initialization_Test()
        {
            string name = "Casino chips";
            string sign = "$";
            string code = "CC";

            Currency chips = new Currency(code, name, sign);

            Assert.Equal(chips.Code, code);
            Assert.Equal(chips.Sign, sign);
            Assert.Equal(chips.Name, name);
        }
    }
}
