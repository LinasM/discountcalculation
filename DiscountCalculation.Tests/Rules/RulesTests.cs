﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.Rules;
using System;
using System.Collections.Generic;
using Xunit;

namespace DiscountCalculation.Tests.Rules
{
    public class RulesTests
    {
        readonly Currency euro;
        readonly IDataProvider dataProvider = new MockDataProvider();

        readonly Carrier.SizeCode sizeSmall = Carrier.SizeCode.S;
        readonly Carrier.SizeCode sizeMedium = Carrier.SizeCode.M;
        readonly Carrier.SizeCode sizeLarge = Carrier.SizeCode.L;

        readonly Dictionary<Carrier.SizeCode, Money> prices;
        readonly Carrier rickshaw;
        readonly Carrier wrongCarrier;

        readonly List<Transaction> oldTransactions;

        public RulesTests()
        {
            euro = dataProvider.GetCurrency("EUR");

            prices = new Dictionary<Carrier.SizeCode, Money>
            {
                {sizeSmall, new Money(25.00m, euro)},
                {sizeMedium, new Money(50.00m, euro)},
                {sizeLarge, new Money(100.00m, euro)},
            };

            rickshaw = new Carrier("Rickshaw Transcontinental", "RT", prices);
            wrongCarrier = new Carrier("Wrong Carrier", "WC", prices);

            oldTransactions = new List<Transaction>
            {
                new Transaction(new DateTime(2001, 1, 04), sizeSmall, rickshaw),
                new Transaction(new DateTime(2001, 3, 04), sizeSmall, rickshaw),
                new Transaction(new DateTime(2002, 7, 18), sizeSmall, rickshaw),
                new Transaction(new DateTime(2002, 6, 12), sizeSmall, rickshaw),
                new Transaction(new DateTime(2003, 11, 28), sizeSmall, rickshaw),

                new Transaction(new DateTime(2016, 1, 1), sizeSmall, rickshaw),
                new Transaction(new DateTime(2016, 2, 1), sizeSmall, rickshaw),
                new Transaction(new DateTime(2016, 3, 1), sizeSmall, rickshaw),
            };
        }

        [Fact]
        public void RulesList_TestIfOrderMatters()
        {
            Money lowestPriceS = new Money(0.20m, euro);
            IHistory history = new HistoryInMemory();
            Carrier laPoste = dataProvider.GetCarrier("LP");
            Money discountLimit = new Money(100000.00m, euro);
            (DateRange.Period, Money) limit = (DateRange.Period.Month, discountLimit);

            MatchGivenPriceRule orderTestRule = new MatchGivenPriceRule("orderTestRule", lowestPriceS, isFinalDiscount: false);
            NthTransactionRule orderTestRuleFinal = new NthTransactionRule("orderTestRuleFinal", discountCoeff: 0.0m, which: 1,
                onlyOnce: true, period: DateRange.Period.Month, history);

            orderTestRule.AppliesTo(Carrier.SizeCode.S);
            orderTestRule.AppliesTo(laPoste);
            orderTestRuleFinal.AppliesTo(Carrier.SizeCode.S);
            orderTestRuleFinal.AppliesTo(laPoste);


            List<IRule> rules = new List<IRule>
            {
                orderTestRule,
                orderTestRuleFinal
            };

            List<IRule> rulesReverseOrder = new List<IRule>
            {
                orderTestRuleFinal,
                orderTestRule
            };

            Transaction testTransaction = new Transaction(DateTime.Now, Carrier.SizeCode.S, laPoste);
            DiscountManager testManager = new DiscountManager(dataProvider.GetHistory(), limit, euro);
            testManager.SetRules(rules);

            Dictionary<string, Money> discounts1 = testManager.GetDiscounts(testTransaction);
            testManager.SetRules(rulesReverseOrder);
            Dictionary<string, Money> discounts2 = testManager.GetDiscounts(testTransaction);
            testManager.SetRules(rules);
            Dictionary<string, Money> discounts3 = testManager.GetDiscounts(testTransaction);

            int actualCount1 = discounts1.Count; // 2
            int actualCount2 = discounts2.Count; // 1
            int actualCount3 = discounts3.Count; // 2

            int expectedCount1 = 2;
            int expectedCount2 = 1;
            int expectedCount3 = 2;

            Assert.Equal(expectedCount1, actualCount1);
            Assert.Equal(expectedCount2, actualCount2);
            Assert.Equal(expectedCount3, actualCount3);
        }

        [Fact]
        public void NthTransactionRule_OnlyOnce()
        {
            IHistory history = new HistoryInMemory();
            history.SaveTransactions(oldTransactions);


            NthTransactionRule ruleSecondShipmentPerYear = new NthTransactionRule("testNth", discountCoeff: 0.20m, which: 2,
                onlyOnce: true, DateRange.Period.Year, history);

            ruleSecondShipmentPerYear.AppliesTo(sizeSmall);
            ruleSecondShipmentPerYear.AppliesTo(rickshaw);

            Transaction testTransaction1 = new Transaction(new DateTime(2001, 10, 25), sizeSmall, rickshaw);
            Transaction testTransaction2 = new Transaction(new DateTime(2003, 1, 1), Carrier.SizeCode.L, rickshaw);
            Transaction testTransaction3 = new Transaction(new DateTime(2003, 1, 2), sizeSmall, rickshaw);
            Transaction testTransaction4 = new Transaction(new DateTime(2002, 1, 1), sizeSmall, rickshaw);
            Transaction testTransaction5 = new Transaction(new DateTime(2002, 6, 13), sizeSmall, wrongCarrier);
            Transaction testTransaction6 = new Transaction(new DateTime(2003, 12, 1), sizeSmall, rickshaw);

            // Note that test transactions aren't saved to history they dont influence subsequent tests
            Assert.False(ruleSecondShipmentPerYear.IsEligible(testTransaction1)); // Not 3d this year, must be 2nd
            Assert.False(ruleSecondShipmentPerYear.IsEligible(testTransaction2)); // 2nd this year but wrong size code
            Assert.True(ruleSecondShipmentPerYear.IsEligible(testTransaction3)); // 2nd this year, correct size code, correct carrier, hooray
            Assert.False(ruleSecondShipmentPerYear.IsEligible(testTransaction4)); // 1st this year, must be second
            Assert.False(ruleSecondShipmentPerYear.IsEligible(testTransaction5)); // 2nd this year but wrong carrier
            Assert.True(ruleSecondShipmentPerYear.IsEligible(testTransaction6)); // 2nd this year, correct size correct carrier. 
        }
        
        [Fact]
        public void NthTransactionRule_EverySecondOne()
        {
            IHistory history = new HistoryInMemory();
            history.SaveTransactions(oldTransactions);


            NthTransactionRule ruleEverySecondShipmentPerYear = new NthTransactionRule("testEveryNth", discountCoeff: 0.20m, which: 2,
                onlyOnce: false, DateRange.Period.Year, history);
            ruleEverySecondShipmentPerYear.AppliesTo(rickshaw);
            ruleEverySecondShipmentPerYear.AppliesTo(sizeSmall);

            Transaction testTransaction7 = new Transaction(new DateTime(2016, 4, 1), sizeSmall, rickshaw);
            Transaction testTransaction8 = new Transaction(new DateTime(2016, 1, 2), sizeSmall, rickshaw);
            Transaction testTransaction9 = new Transaction(new DateTime(2001, 12, 30), sizeSmall, rickshaw);

            // Note that test transactions aren't saved to history they dont influence subsequent tests
            Assert.True(ruleEverySecondShipmentPerYear.IsEligible(testTransaction7)); // 4th this year meaning 2nd 2nd time
            Assert.True(ruleEverySecondShipmentPerYear.IsEligible(testTransaction8)); // 2nd
            Assert.False(ruleEverySecondShipmentPerYear.IsEligible(testTransaction9)); // 3d.
        }
    }
}
