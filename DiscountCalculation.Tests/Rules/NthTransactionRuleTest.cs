﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.Rules;
using System;
using Xunit;

namespace DiscountCalculation.Tests.Rules
{
    public class NthTransactionRuleTest
    {
        IDataProvider dataProvider;

        DateTime oldDateAugust = new DateTime(2020, 8, 1);
        DateTime oldDateAugustAgain = new DateTime(2020, 8, 2);
        DateTime veryOldDate = new DateTime(2019, 8, 1);

        Transaction oldTransactionAugust;
        Transaction oldTransactionAugustLastYear;
        Transaction oldTransactionAugustAgain;

        public NthTransactionRuleTest()
        {
            dataProvider = new MockDataProvider();

            oldTransactionAugust = new Transaction(oldDateAugust, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));
            oldTransactionAugustLastYear = new Transaction(veryOldDate, Carrier.SizeCode.L, dataProvider.GetCarrier("MR"));
            oldTransactionAugustAgain = new Transaction(oldDateAugustAgain, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));
        }


        [Fact]
        public void IsEligible_YesItIsTest()
        {

            IHistory mockHistory = new HistoryInMemory();
            mockHistory.SaveTransaction(oldTransactionAugust);
            mockHistory.SaveTransaction(oldTransactionAugustLastYear);

            DateTime testDate = new DateTime(2020, 8, 3);


            Transaction testTransaction = new Transaction(testDate, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));


            NthTransactionRule secondEveryMonthRule = new NthTransactionRule("3dLFreeOncePerMonthTest", 
                discountCoeff: 0.5m, 
                which: 2,
                onlyOnce: true, 
                period: DateRange.Period.Month, 
                mockHistory);

            secondEveryMonthRule.AppliesTo(Carrier.SizeCode.S);
            secondEveryMonthRule.AppliesTo(dataProvider.GetCarrier("LP"));

            bool isEligible = secondEveryMonthRule.IsEligible(testTransaction);

            Assert.True(isEligible);
        }

        [Fact]
        public void IsEligible_NoLongerIsTest()
        {
            IHistory mockHistory = new HistoryInMemory();
            mockHistory.SaveTransaction(oldTransactionAugust);
            mockHistory.SaveTransaction(oldTransactionAugustLastYear);
            mockHistory.SaveTransaction(oldTransactionAugustAgain);

            DateTime testDate = new DateTime(2020, 8, 3);


            Transaction testTransaction = new Transaction(testDate, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));

            NthTransactionRule secondEveryMonthRule = new NthTransactionRule("3dLFreeOncePerMonthTest",
                discountCoeff: 0.5m,
                which: 2,
                onlyOnce: true,
                period: DateRange.Period.Month,
                mockHistory);

            secondEveryMonthRule.AppliesTo(Carrier.SizeCode.S);
            secondEveryMonthRule.AppliesTo(dataProvider.GetCarrier("LP"));

            bool isEligible = secondEveryMonthRule.IsEligible(testTransaction);

            Assert.False(isEligible);
        }

        [Fact]
        public void IsEligible_WrongCarrierTest()
        {
            IHistory mockHistory = new HistoryInMemory();
            mockHistory.SaveTransaction(oldTransactionAugust);
            mockHistory.SaveTransaction(oldTransactionAugustLastYear);

            DateTime testDate = new DateTime(2020, 8, 3);

            Transaction testTransaction = new Transaction(testDate, Carrier.SizeCode.S, dataProvider.GetCarrier("MR"));

            NthTransactionRule secondEveryMonthRule = new NthTransactionRule("3dLFreeOncePerMonthTest",
                discountCoeff: 0.5m,
                which: 2,
                onlyOnce: true,
                period: DateRange.Period.Month,
                mockHistory);

            secondEveryMonthRule.AppliesTo(Carrier.SizeCode.S);
            secondEveryMonthRule.AppliesTo(dataProvider.GetCarrier("LP"));

            bool isEligible = secondEveryMonthRule.IsEligible(testTransaction);
            Assert.False(isEligible);
        }

        [Fact]
        public void isEligible_WrongSizeTest()
        {
            IHistory mockHistory = new HistoryInMemory();
            mockHistory.SaveTransaction(oldTransactionAugust);
            mockHistory.SaveTransaction(oldTransactionAugustLastYear);

            DateTime testDate = new DateTime(2020, 8, 3);

            Transaction testTransaction = new Transaction(testDate, Carrier.SizeCode.L, dataProvider.GetCarrier("LP"));

            NthTransactionRule secondEveryMonthRule = new NthTransactionRule("3dLFreeOncePerMonthTest",
                discountCoeff: 0.5m,
                which: 2,
                onlyOnce: true,
                period: DateRange.Period.Month,
                mockHistory);

            secondEveryMonthRule.AppliesTo(Carrier.SizeCode.S);
            secondEveryMonthRule.AppliesTo(dataProvider.GetCarrier("LP"));

            bool isEligible = secondEveryMonthRule.IsEligible(testTransaction);
            Assert.False(isEligible);
        }
    }
}
