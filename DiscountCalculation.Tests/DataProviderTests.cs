﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using System;
using System.Collections.Generic;
using Xunit;

namespace DiscountCalculation.Tests
{
    public class DataProviderTests
    {
        [Fact]
        public void GetLowestPrice_NotNull()
        {
            IDataProvider dataProvider = new MockDataProvider();

            Money lowestPriceS = dataProvider.GetLowestPrice(Carrier.SizeCode.S);
            Money lowestPriceM = dataProvider.GetLowestPrice(Carrier.SizeCode.M);
            Money lowestPriceL = dataProvider.GetLowestPrice(Carrier.SizeCode.L);

            Assert.NotNull(lowestPriceS);
            Assert.NotNull(lowestPriceM);
            Assert.NotNull(lowestPriceL);
        }

        [Fact]
        public void Initialization_Test()
        {
            IDataProvider dataProvider = new MockDataProvider();

            Assert.NotNull(dataProvider.GetHistory());
            Assert.NotNull(dataProvider.GetAvailableCarriers());
        }

        [Fact]
        public void GetCarrier_InvalidCarrierCodeTest()
        {
            IDataProvider dataProvider = new MockDataProvider();
            string carrierCode = "A carrier code that does not exist nor ever will";

            Assert.Throws<ArgumentException>(() => dataProvider.GetCarrier(carrierCode));
        }


        [Fact]
        public void GetLowestPrice_IsLowest()
        {
            IDataProvider dataProvider = new MockDataProvider();

            Money lowestPriceS = dataProvider.GetLowestPrice(Carrier.SizeCode.S);
            Money lowestPriceM = dataProvider.GetLowestPrice(Carrier.SizeCode.M);
            Money lowestPriceL = dataProvider.GetLowestPrice(Carrier.SizeCode.L);

            bool isLowest = true;
            foreach (KeyValuePair<string, Carrier> keyValuePair in dataProvider.GetAvailableCarriers())
            {
                if ( keyValuePair.Value.Prices[Carrier.SizeCode.S] < lowestPriceS 
                    || keyValuePair.Value.Prices[Carrier.SizeCode.M] < lowestPriceM
                    || keyValuePair.Value.Prices[Carrier.SizeCode.L] < lowestPriceL)
                {
                    isLowest = false;
                    break;
                }
            }

            Assert.True(isLowest);
        }
    }
}
