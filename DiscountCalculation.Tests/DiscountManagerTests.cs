﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.Rules;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Xunit;

namespace DiscountCalculation.Tests
{
    public class DiscountManagerTests
    {
        Currency euro = new Currency("EUR", "Euro", "€");
        MockDataProvider dataProvider = new MockDataProvider();

        [Fact]
        public void GetAccumulatedDiscounts_Test()
        {
            decimal priceAmount = 5.00m;
            decimal discountLimitAmount = 20.00m;
            decimal discountAmount = 8.00m;

            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                {Carrier.SizeCode.L, new Money(priceAmount, euro)},
            };
            Carrier mockCarrier = new Carrier("Bulgarijos kroviniai", "BK", prices);

            DateTime date = new DateTime(2020, 8, 5); // Wednesday
            DateTime historicalDate = new DateTime(2020, 8, 4); // Tuesday same week
            Transaction historicalTransaction = new Transaction(historicalDate, Carrier.SizeCode.L, mockCarrier);
            historicalTransaction.AddDiscount("fakeRule", new Money(discountAmount, euro));

            IHistory history = new HistoryInMemory();
            history.SaveTransaction(historicalTransaction);

            Money discountLimit = new Money(discountLimitAmount, euro);
            DateRange.Period period = DateRange.Period.Week;
            (DateRange.Period, Money) limit = (period, discountLimit);
            DiscountManager discountManager = new DiscountManager(history, limit, euro);

            Money actualAccumulatedDiscount = discountManager.GetAccumulatedDiscounts(date);
            Money expectedAccumulatedDiscount = new Money(discountAmount, euro);

            Assert.True(expectedAccumulatedDiscount == actualAccumulatedDiscount);
        }

        [Fact]
        public void GetDiscount_TestDiscountsDisabled()
        {
            Money discountLimit = new Money(0.0m, euro);
            (DateRange.Period, Money) limit = (DateRange.Period.Month, discountLimit);

            DiscountManager discountManager = new DiscountManager(dataProvider.GetHistory(), limit, euro);

            Transaction transaction1 = new Transaction(DateTime.Now, Carrier.SizeCode.S, dataProvider.GetCarrier("LP"));
            Transaction transaction2 = new Transaction(DateTime.Now, Carrier.SizeCode.S, dataProvider.GetCarrier("MR"));

            decimal expectedDiscount = 0.0m;

            Dictionary<string, Money> transaction1Discounts = discountManager.GetDiscounts(transaction1);
            Dictionary<string, Money> transaction2Discounts = discountManager.GetDiscounts(transaction2);

            decimal actualTransaction1Discount = transaction1Discounts.Sum(x => x.Value.Amount);
            decimal actualTransaction2Discount = transaction2Discounts.Sum(x => x.Value.Amount);

            Assert.True(actualTransaction1Discount == expectedDiscount);
            Assert.True(actualTransaction2Discount == expectedDiscount);
        }

        [Fact]
        public void GetDiscounts_LessOrEqualLimit()
        {
            Money discountLimit = new Money(1.00m, euro);
            Money givenPrice = new Money(0.25m, euro);
            MatchGivenPriceRule rule = new MatchGivenPriceRule("testRule", givenPrice);
            (DateRange.Period, Money) limit = (DateRange.Period.Month, discountLimit);
            Dictionary<Carrier.SizeCode, Money> prices = new Dictionary<Carrier.SizeCode, Money>
            {
                {Carrier.SizeCode.M, new Money(10.00m, euro)},
            };
            Carrier mockCarrier = new Carrier("Bulgarijos kroviniai", "BK", prices);

            rule.AppliesTo(mockCarrier);
            rule.AppliesTo(Carrier.SizeCode.M);

            DiscountManager discountManager = new DiscountManager(rule, new HistoryInMemory(), limit, euro);
            Transaction transaction = new Transaction(DateTime.Now, Carrier.SizeCode.M, mockCarrier);

            decimal actualDiscountAmount = discountManager.GetDiscounts(transaction).Sum(x => x.Value.Amount);

            Assert.True(discountLimit.Amount > 0); // just to check if discount as applied at all
            Assert.True(discountLimit.Amount >= actualDiscountAmount);
        }
    }
}
