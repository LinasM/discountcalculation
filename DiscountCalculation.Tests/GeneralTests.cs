﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.IO;
using DiscountCalculation.Rules;
using System.Collections.Generic;
using Xunit;

namespace DiscountCalculation.Tests
{
    public class GeneralTests
    {
        /// <summary>
        /// Tests if the program works correctly with default data provided in the task description.
        /// I.e. if given the input provided in the task description, we get the output provided in the task description.
        /// 
        /// This is not exactly a _unit_ test and not a serious test at all. But it's somewhat convenient to have.
        /// It's here so I wouldn't need to manually compare results each time :)
        /// 
        /// Passing this test does not necessarily mean app is gonna work with all data but it's a good start.
        /// Failing this test does not necessarily mean code is bad. It can fail even due to different locales 
        /// representing decimal separator differently.
        /// </summary>
        [Fact]
        public void ProvidedData_Test()
        {
            #region initialize
            string filePath = "input.txt";
            char delimiter = ' ';

            ListBufferMessenger messenger = new ListBufferMessenger();
            IDataProvider dataProvider = new MockDataProvider();
            IRulesFactory rulesFactory = new RulesFactory(dataProvider);
            Currency defaultCurrency = dataProvider.GetCurrency("EUR");
            Money discountLimit = new Money(10.00m, defaultCurrency);
            (DateRange.Period, Money) limit = (DateRange.Period.Month, discountLimit);

            DiscountManager discountManager = new DiscountManager(rulesFactory.GetRules(), dataProvider.GetHistory(), 
                limit, defaultCurrency);
            #endregion

            Parser parser = new Parser(dataProvider, messenger);
            parser.TryParse(filePath, delimiter, discountManager);

            List<string> actualOutput = messenger.Buffer;
            List<string> expectedOutput = new List<string>
            {
                "2015-02-01 S MR 1.50 0.50",
                "2015-02-02 S MR 1.50 0.50",
                "2015-02-03 L LP 6.90 -",
                "2015-02-05 S LP 1.50 -",
                "2015-02-06 S MR 1.50 0.50",
                "2015-02-06 L LP 6.90 -",
                "2015-02-07 L MR 4.00 -",
                "2015-02-08 M MR 3.00 -",
                "2015-02-09 L LP 0.00 6.90",
                "2015-02-10 L LP 6.90 -",
                "2015-02-10 S MR 1.50 0.50",
                "2015-02-10 S MR 1.50 0.50",
                "2015-02-11 L LP 6.90 -",
                "2015-02-12 M MR 3.00 -",
                "2015-02-13 M LP 4.90 -",
                "2015-02-15 S MR 1.50 0.50",
                "2015-02-17 L LP 6.90 -",
                "2015-02-17 S MR 1.90 0.10",
                "2015-02-24 L LP 6.90 -",
                "2015-02-29 CUSPS Ignored",
                "2015-03-01 S MR 1.50 0.50",
            };

            Assert.Equal(expectedOutput, actualOutput);
        }
    }
}
