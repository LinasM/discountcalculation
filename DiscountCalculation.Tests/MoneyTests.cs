﻿using DiscountCalculation.Data;
using DiscountCalculation.Entity;
using DiscountCalculation.IO;
using Xunit;
using Assert = Xunit.Assert;


namespace DiscountCalculation.Tests
{
    public class MoneyTests
    {
        Currency euros = new Currency("EUR", "Euro", "€");
        Currency pounds = new Currency("GBP", "Pounds", "£");

        Money sumInEuros100;
        Money sumInEuros80;
        Money sumInEuros80again;

        Money sumInPounds120;


        public MoneyTests()
        {
            sumInEuros100 = new Money(100, euros);
            sumInEuros80 = new Money(80, euros);
            sumInEuros80again = new Money(80, euros);

            sumInPounds120 = new Money(120, pounds);
        }


        [Fact]
        public void ComparisonOperators_TestSameCurrency()
        {
            Assert.True(sumInEuros80 == sumInEuros80again);
            Assert.True(sumInEuros100 != sumInEuros80);

            Assert.True(sumInEuros100 > sumInEuros80);
            Assert.True(sumInEuros80again < sumInEuros100);

            Assert.True(sumInEuros80again <= sumInEuros100);
            Assert.True(sumInEuros100 >= sumInEuros80);
        }

        [Fact]
        public void SubtractionOperator_TestSameCurrency()
        {
            Money expectedlValue = new Money(20, euros);
            Money actualValue = sumInEuros100 - sumInEuros80;

            Assert.True(expectedlValue == actualValue);
            Assert.True(expectedlValue.Amount == actualValue.Amount);
            Assert.True(expectedlValue.Currency.Equals(actualValue.Currency));
        }

        [Fact]
        public void NeedsNormalizing_No()
        {
            IDataProvider dataProvider = new MockDataProvider();

            Currency euros = dataProvider.GetCurrency("EUR");
            Currency pounds = dataProvider.GetCurrency("GBP");

            Money money1 = Money.Zero;

            Money money2 = new Money(10.0m, this.euros);
            Money money3 = new Money(1.0m, this.euros);            
            Money money4 = new Money(10.0m, pounds);

            Money money5 = dataProvider.GetCarrier("LP").Prices[Carrier.SizeCode.S];
            Money money6 = dataProvider.GetCarrier("MR").Prices[Carrier.SizeCode.S];

            Money money7 = new Money(7.0m, euros);

            Assert.False(Money.NeedsNormalizing(money1, money2));
            Assert.False(Money.NeedsNormalizing(money2, money3));
            Assert.False(Money.NeedsNormalizing(money4, money1));
            Assert.False(Money.NeedsNormalizing(money5, money1));
            Assert.False(Money.NeedsNormalizing(money6, money1));
            Assert.False(Money.NeedsNormalizing(money6, money5));
            Assert.False(Money.NeedsNormalizing(money7, money1));
            Assert.False(Money.NeedsNormalizing(money7, money2));
        }

        [Fact]
        public void MoneyZero_Test()
        {
            Money money = Money.Zero;

            decimal expectedAmount = 0.0m;
            decimal actualAmount = money.Amount;

            Assert.NotNull(money);
            Assert.Equal(actualAmount, expectedAmount);
            Assert.Null(money.Currency);
        }

        //[Fact]
        //public void Operators_TestDifferentCurrency()
        //{
        //    Assert.True(sumInEuros100 != sumInPounds120);
        //}
    }
}
